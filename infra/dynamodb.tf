locals {
  summary_table_name = "${var.env}-db-dyntable-${var.project_code}-summarytable"
}

resource "aws_dynamodb_table" "summary_table" {
  name         = local.summary_table_name
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "WeekId"
  range_key    = "DayId"

  attribute {
    name = "WeekId"
    type = "S"
  }

  attribute {
    name = "DayId"
    type = "N"
  }

  tags = {
    Name        = local.summary_table_name
    Environment = var.env
    ProjectCode = var.project_code
    Zone        = "db"
  }
}
