output "function_latest_version" {
  value = module.lambda_function.function.version
}

output "function_name" {
  value = module.lambda_function.function.function_name
}
