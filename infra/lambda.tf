locals {
  execution_role_name        = "${var.env}-mgmt-role-${var.project_code}"
  execution_role_policy_name = "${var.env}-mgmt-policy-${var.project_code}"
  function_name              = "${var.env}-app-func-${var.project_code}"
}


module "function_execution_role" {
  source = "./modules/iam_role"
  name   = local.execution_role_name
  policy_attachments = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
  ]

  custom_policy = {
    name = local.execution_role_policy_name
    statements = {
      allowDynamodb = {
        effect = "Allow"
        actions = [
          "dynamodb:PutItem",
          "dynamodb:DeleteItem",
          "dynamodb:GetItem",
          "dynamodb:Query",
        ]
        resources = ["*"]
      }
    }
  }
}

module "lambda_function" {
  source                            = "./modules/lambda_function"
  function_name                     = local.function_name
  ignore_deployment_package_changes = true

  execution_role_arn = module.function_execution_role.role.arn
  deployment_package = {
    filename         = data.archive_file.deployment_package.output_path
    source_code_hash = data.archive_file.deployment_package.output_base64sha256
  }
  runtime = "provided.al2"
  handler = "bootstrap"

  vpc_config = {
    subnet_ids         = [data.aws_subnet.private.id]
    security_group_ids = [data.aws_security_group.allow_nat.id]
  }

  environment_variables = {
    REDIS_ADDR         = "globalrediscache.internal.globalvpc:6379"
    REDIS_KEY          = var.project_code
    SUMMARY_TABLE_NAME = aws_dynamodb_table.summary_table.name
    TOKEN              = data.aws_ssm_parameter.token.value
  }
}


data "archive_file" "deployment_package" {
  type       = "zip"
  source_dir = "${path.module}/${var.deployment_package_location}"
  excludes   = ["${path.module}/${var.deployment_package_location}/.gitkeep"]

  output_path = "${path.module}/../deploy/bootstrap.zip"
}
