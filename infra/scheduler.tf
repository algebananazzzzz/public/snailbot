locals {
  scheduler_execution_role_name        = "${var.env}-mgmt-role-${var.project_code}-scheduler"
  scheduler_execution_role_policy_name = "${var.env}-mgmt-policy-${var.project_code}-scheduler"
  scheduler_group_name                 = "${var.env}-app-schedulegrp-${var.project_code}"
  schedule_daily_morning               = "${var.env}-app-schedule-${var.project_code}-daily-8"
  schedule_daily_lunch                 = "${var.env}-app-schedule-${var.project_code}-daily-12"
  schedule_daily_dinner                = "${var.env}-app-schedule-${var.project_code}-daily-19"
  schedule_daily_night                 = "${var.env}-app-schedule-${var.project_code}-daily-23"
}

module "scheduler_execution_role" {
  source = "./modules/iam_role"
  name   = local.scheduler_execution_role_name

  assume_role_allowed_principals = [{
    type        = "Service"
    identifiers = ["scheduler.amazonaws.com"]
  }]

  custom_policy = {
    name = local.scheduler_execution_role_policy_name
    statements = {
      allowLambdaInvoke = {
        effect = "Allow"
        actions = [
          "lambda:InvokeFunction",
        ]
        resources = [module.lambda_function.function.arn]
      }
    }
  }
}

resource "aws_scheduler_schedule_group" "this" {
  name = local.scheduler_group_name
}

resource "aws_scheduler_schedule" "daily_morning" {
  name       = local.schedule_daily_morning
  group_name = aws_scheduler_schedule_group.this.name

  flexible_time_window {
    mode = "OFF"
  }
  schedule_expression          = "cron(0 0 * * ? *)"
  schedule_expression_timezone = "UTC"


  target {
    arn      = module.lambda_function.function.arn
    role_arn = module.scheduler_execution_role.role.arn

    input = jsonencode({
      command = "send_poll_daily"
    })
  }
}

resource "aws_scheduler_schedule" "daily_lunch" {
  name       = local.schedule_daily_lunch
  group_name = aws_scheduler_schedule_group.this.name

  flexible_time_window {
    mode = "OFF"
  }
  schedule_expression          = "cron(0 4 * * ? *)"
  schedule_expression_timezone = "UTC"


  target {
    arn      = module.lambda_function.function.arn
    role_arn = module.scheduler_execution_role.role.arn

    input = jsonencode({
      command = "send_love_message"
    })
  }
}

resource "aws_scheduler_schedule" "daily_dinner" {
  name       = local.schedule_daily_dinner
  group_name = aws_scheduler_schedule_group.this.name

  flexible_time_window {
    mode = "OFF"
  }
  schedule_expression          = "cron(0 11 * * ? *)"
  schedule_expression_timezone = "UTC"


  target {
    arn      = module.lambda_function.function.arn
    role_arn = module.scheduler_execution_role.role.arn

    input = jsonencode({
      command = "send_love_message"
    })
  }
}

resource "aws_scheduler_schedule" "daily_night" {
  name       = local.schedule_daily_night
  group_name = aws_scheduler_schedule_group.this.name

  flexible_time_window {
    mode = "OFF"
  }
  schedule_expression          = "cron(0 15 * * ? *)"
  schedule_expression_timezone = "UTC"

  target {
    arn      = module.lambda_function.function.arn
    role_arn = module.scheduler_execution_role.role.arn

    input = jsonencode({
      command = "generate_summary_daily"
    })
  }
}
