data "aws_ssm_parameter" "token" {
  name = "/${var.env}/app/${var.project_code}/TOKEN"
}
