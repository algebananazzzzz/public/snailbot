package main

import (
	"fmt"
	"log"
	"math/rand"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func handleError(chatId int64, err error) error {
	msg := tgbotapi.NewMessage(chatId, fmt.Sprintf("%v\nError: %v", unexpectedErrorResponse(), err))
	bot.Send(msg)
	return err
}

func deletePollDataForChat(data ChatData) error {
	pollData, err := getPollData(data.ActivityPollId)
	if err != nil {
		return err
	}

	stopPollMsg := tgbotapi.NewStopPoll(data.ChatId, pollData.MessageId)
	_, err = bot.Send(stopPollMsg)
	if err != nil {
		return err
	}

	pollData, err = getPollData(data.ExercisePollId)
	if err != nil {
		return err
	}

	stopPollMsg = tgbotapi.NewStopPoll(data.ChatId, pollData.MessageId)
	_, err = bot.Send(stopPollMsg)
	if err != nil {
		return err
	}

	if err := deletePollData(data.ActivityPollId); err != nil {
		return err
	}

	if err := deletePollData(data.ExercisePollId); err != nil {
		return err
	}

	if err := clearChatData(data.ChatId); err != nil {
		return err
	}
	return nil
}

func generateSummaryText(data WeekSummaryItem) (string, int) {
	msgText := "<b>Activities done:</b>\n"
	activityPoints := 0
	for index, number := range data.ActivityOptions {
		msgText += fmt.Sprintf("%v (%v)\n", POLL_ACTIVITY_OPTIONS[index].Option, number)
		activityPoints += POLL_ACTIVITY_OPTIONS[index].Points * number
	}
	msgText += fmt.Sprintln("<b>Points:</b>", activityPoints)

	msgText += "\n<b>Exercises done:</b>\n"
	exercisePoints := 0
	for index, number := range data.ExerciseOptions {
		msgText += fmt.Sprintf("%v (%v)\n", POLL_EXERCISE_OPTIONS[index].Option, number)
		exercisePoints += POLL_EXERCISE_OPTIONS[index].Points * number
	}
	totalPoints := activityPoints + exercisePoints
	msgText += fmt.Sprintf("<b>Points:</b> %v\n\n<b>Total Points:</b> %v", activityPoints, totalPoints)

	return msgText, totalPoints
}

func generateSummaryDaily(data ChatData, weekId string) (*string, *WeekSummaryItem, error) {
	summaryItem := WeekSummaryItem{
		WeekId: weekId,
		DayId:  WEEK_DAY_NOW,
	}
	msgText := fmt.Sprintf("<b>Summary %v</b>:\n\n", TIME_NOW.Format("Mon, Jan 2"))

	var err error
	if summaryItem.ActivityOptions, err = getPollOptionsSummary(data.ActivityPollId); err != nil {
		return nil, nil, err
	}
	if summaryItem.ExerciseOptions, err = getPollOptionsSummary(data.ExercisePollId); err != nil {
		return nil, nil, err
	}

	summaryText, totalPoints := generateSummaryText(summaryItem)
	summaryItem.Points = totalPoints
	msgText += summaryText

	return &msgText, &summaryItem, nil
}

func getPollOptionsSummary(pollId string) (map[int]int, error) {
	optionsData := make(map[int]int)
	pollData, err := getPollData(pollId)
	if err != nil {
		return nil, err
	}
	if len(pollData.SelectedOptions) == 0 {
		return optionsData, nil
	}
	for _, index := range pollData.SelectedOptions {
		optionsData[index] = 1
	}
	return optionsData, nil
}

func handleInitNewPoll(chatId int64, pollType int) (*string, error) {
	var pollQuestion string
	var pollOptions []string

	switch pollType {
	case POLL_ACTIVITY_TYPE:
		pollQuestion = POLL_ACTIVITY_QUESTION

		for _, v := range POLL_ACTIVITY_OPTIONS {
			pollOptions = append(pollOptions, v.Option)
		}
	case POLL_EXERCISE_TYPE:
		pollQuestion = POLL_EXERCISE_QUESTION

		for _, v := range POLL_EXERCISE_OPTIONS {
			pollOptions = append(pollOptions, v.Option)
		}
	default:
		return nil, fmt.Errorf("unknown poll type: %v", pollType)
	}

	pollMsg := tgbotapi.NewPoll(chatId, pollQuestion, pollOptions...)
	pollMsg.IsAnonymous = false
	pollMsg.AllowsMultipleAnswers = true

	response, err := bot.Send(pollMsg)
	if err != nil {
		return nil, err
	}
	log.Print(response)
	newPollData := PollData{
		Id:        response.Poll.ID,
		Type:      POLL_ACTIVITY_TYPE,
		MessageId: response.MessageID,
	}

	if err := dumpPollData(newPollData); err != nil {
		log.Fatal(err)
	}
	return &response.Poll.ID, nil
}

func promptResponse() string {
	responses := [5]string{
		"What kind of snail-paced adventure would you like to embark on today? 🐌",
		"I'm here to shed some snailluminated insights! What's on your mind? 🐌",
		"At a snail's pace, I'm ready to assist. What can I help you with today? 🐌",
		"I may move at a snail's pace, but I'm snail-initely here to assist you! What do you wanna do today? 🐌",
		"I'm all ears, or rather, antennas! 🐌 What's your next move?",
	}
	return responses[rand.Intn(len(responses))]
}

func unexpectedErrorResponse() string {
	responses := [5]string{
		"My snail senses are tingling, it seems that I've encountered an error. 🚨",
		"It appears I've encountered a snail roadblock and got stuck. 🚨",
		"My snail brain is a bit befuddled by an unexpected error. 🚨",
		"I'm as lost as a snail in a salt maze over what went wrong. 🚨",
		"I'm sorry, but I had to hide in my shell after encountering such a huge bug. 🚨",
	}
	response := responses[rand.Intn(len(responses))]
	response += " The admin has been notified of this error, and will run diagnostics on it."
	return response
}

func generateReminderMessage() string {
	responses := [5]string{
		"Don't forget to send each other a little 'snail mail'! 🐌💕",
		"Snailbot says: Take a pause from your busy day and send a snuggly message to your partner! 🐌💌",
		"Time slip away faster than a snail's pace! Drop a text to your significant other right now! 🐌📲",
		"Snail and steady wins the race, but a quick 'hello' to your special someone wouldn't hurt! 🐌📱",
		"Snailbot's little nudge: Text your partner and let them know they're on your mind! 🐌💬",
	}
	return responses[rand.Intn(len(responses))]
}
