package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/redis/go-redis/v9"
)

func clearAllData() error {
	return redisClient.Del(REDIS_KEY).Err()
}

func getPollData(pollId string) (PollData, error) {
	pollKey := fmt.Sprintf("poll:%v", pollId)
	redisData, err := redisClient.HGet(REDIS_KEY, pollKey).Result()
	log.Println("GET_POLL_DATA_RESULT", pollKey, redisData)

	if err != nil {
		log.Println("GET_POLL_DATA_ERROR", err)
		return PollData{Id: pollId}, err
	}

	var data PollData

	if err := json.Unmarshal([]byte(redisData), &data); err != nil {
		log.Println("GET_POLL_DATA_ERROR", err)
		return PollData{Id: pollId}, err
	}

	return data, nil
}

func dumpPollData(data PollData) error {
	pollKey := fmt.Sprintf("poll:%v", data.Id)
	pollDataBytes, err := json.Marshal(data)
	if err != nil {
		log.Println("DUMP_POLL_DATA_ERROR", pollKey, err)
		return err
	}

	log.Println("DUMP_POLL_DATA", pollKey, string(pollDataBytes))

	err = redisClient.HSet(REDIS_KEY, pollKey, pollDataBytes).Err()
	if err != nil {
		log.Println("DUMP_POLL_DATA_ERROR", pollKey, err)
		return err
	}
	return nil
}

func deletePollData(pollId string) error {
	pollKey := fmt.Sprintf("poll:%v", pollId)
	log.Println("DELETE_POLL_DATA", pollKey)

	if err := redisClient.HDel(REDIS_KEY, pollKey).Err(); err != nil {
		log.Println("DELETE_POLL_DATA_ERROR", pollKey, err)
		return err
	}
	return nil
}

func getChatIds() ([]int64, error) {
	var chatIds []int64
	redisData, err := redisClient.HKeys(REDIS_KEY).Result()
	if err != nil {
		log.Println("GET_CHAT_IDS_ERROR", err)
		return nil, err
	}
	for _, v := range redisData {
		chatId, found := strings.CutPrefix(v, "chat:")
		if found {
			if chatId, err := strconv.ParseInt(chatId, 10, 64); err == nil {
				chatIds = append(chatIds, int64(chatId))
			}
		}
	}
	log.Println("GET_CHAT_IDS", chatIds)

	return chatIds, nil
}

func getChatData(chatID int64) (ChatData, error) {
	chatKey := fmt.Sprintf("chat:%d", chatID)
	redisData, err := redisClient.HGet(REDIS_KEY, chatKey).Result()
	log.Println("GET_CHAT_DATA", chatKey, redisData)

	if err != nil {
		if err.Error() == "redis: nil" {
			log.Println("GET_CHAT_DATA_WARNING", chatKey, err)
			return ChatData{ChatId: chatID, ConvState: DEFAULT_STATE}, nil
		}
		log.Println("GET_CHAT_DATA_ERROR", chatKey, err)
		return ChatData{ChatId: chatID, ConvState: DEFAULT_STATE}, err
	}

	var data ChatData

	if err := json.Unmarshal([]byte(redisData), &data); err != nil {
		log.Println("GET_CHAT_DATA_ERROR", chatKey, err)
		return ChatData{ChatId: chatID, ConvState: DEFAULT_STATE}, err
	}

	return data, nil
}

func dumpChatData(data ChatData) error {
	chatKey := fmt.Sprintf("chat:%d", data.ChatId)
	chatDataBytes, err := json.Marshal(data)
	if err != nil {
		log.Println("DUMP_CHAT_DATA_ERROR", chatKey, err)
		return err
	}

	log.Println("DUMP_CHAT_DATA", chatKey, string(chatDataBytes))

	err = redisClient.HSet(REDIS_KEY, chatKey, chatDataBytes).Err()
	if err != nil && err != redis.Nil {
		log.Println("DUMP_CHAT_DATA_ERROR", chatKey, err)
		return err
	}
	return nil
}

func clearChatData(chatID int64) error {
	chatData := ChatData{
		ChatId:    chatID,
		ConvState: DEFAULT_STATE,
	}

	if err := dumpChatData(chatData); err != nil {
		log.Println("CLEAR_CHAT_DATA_ERROR", chatID, err)
		return err
	}
	log.Println("CLEAR_CHAT_DATA", chatID)
	return nil
}

func deleteChatData(chatId int64) error {
	chatKey := fmt.Sprintf("chat:%d", chatId)

	if err := redisClient.HDel(REDIS_KEY, chatKey).Err(); err != nil {
		log.Println("DELETE_CHAT_DATA_ERROR", chatKey, err)
		return err
	}
	log.Println("DELETE_CHAT_DATA", chatKey)
	return nil
}
