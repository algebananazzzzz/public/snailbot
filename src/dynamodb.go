package main

import (
	"context"
	"log"

	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
)

func PutSummaryItem(item WeekSummaryItem) error {
	log.Println("PUT_SUMMARY_ITEM", item.WeekId, item.DayId)
	dynamodbItem, err := attributevalue.MarshalMap(item)

	if err != nil {
		return err
	}

	_, err = dynamodbClient.PutItem(context.Background(), &dynamodb.PutItemInput{
		TableName: &SUMMARY_TABLE_NAME,
		Item:      dynamodbItem,
	})

	if err != nil {
		return err
	}

	return nil
}

func QuerySummaryItems(weekId string) ([]WeekSummaryItem, error) {
	keyEx := expression.Key("WeekId").Equal(expression.Value(weekId))
	expr, err := expression.NewBuilder().WithKeyCondition(keyEx).Build()
	if err != nil {
		return nil, err
	}

	response, err := dynamodbClient.Query(context.Background(), &dynamodb.QueryInput{
		TableName:                 &SUMMARY_TABLE_NAME,
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		KeyConditionExpression:    expr.KeyCondition(),
	})
	if err != nil {
		return nil, err
	}

	var items []WeekSummaryItem
	if err := attributevalue.UnmarshalListOfMaps(response.Items, &items); err != nil {
		return nil, err
	}
	return items, nil
}
