package main

type CustomEvent struct {
	Command string `json:"command"`
	Payload string `json:"payload"`
}

type CustomEventResponse struct {
	Body string `json:"body"`
}

type ChatData struct {
	ChatId         int64  `json:"chat_id"`
	ConvState      int    `json:"conv_state"`
	ActivityPollId string `json:"activity_poll_id"`
	ExercisePollId string `json:"exercise_poll_id"`
}

type PollData struct {
	Id              string `json:"id"`
	MessageId       int    `json:"message_id"`
	Type            int    `json:"type"`
	SelectedOptions []int  `json:"selected_options"`
}

type WeekSummaryItem struct {
	WeekId          string
	DayId           int
	ActivityOptions map[int]int
	ExerciseOptions map[int]int
	Points          int
}
