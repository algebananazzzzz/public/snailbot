package main

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func customEventHandler(event CustomEvent) (*CustomEventResponse, error) {
	switch event.Command {
	case SEND_POLL_DAILY_COMMAND:
		chatIds, err := getChatIds()
		if err != nil {
			return nil, err
		}

		for _, chatId := range chatIds {
			chatData, err := getChatData(chatId)
			if err != nil {
				return nil, handleError(chatId, err)
			}

			pollId, err := handleInitNewPoll(chatId, POLL_ACTIVITY_TYPE)
			if err != nil {
				return nil, handleError(chatId, err)
			}

			chatData.ActivityPollId = *pollId
			pollId, err = handleInitNewPoll(chatId, POLL_EXERCISE_TYPE)
			if err != nil {
				return nil, handleError(chatId, err)
			}
			chatData.ExercisePollId = *pollId

			if err := dumpChatData(chatData); err != nil {
				return nil, handleError(chatId, err)
			}
		}
	case GENERATE_SUMMARY_DAILY_COMMAND:
		chatIds, err := getChatIds()
		if err != nil {
			return nil, err
		}

		for _, chatId := range chatIds {
			chatData, err := getChatData(chatId)
			if err != nil {
				return nil, handleError(chatId, err)
			}

			weekId := fmt.Sprintf("%v.%v.%v", chatId, YEAR_NOW, WEEK_NOW)
			msgText, summaryItem, err := generateSummaryDaily(chatData, weekId)

			if err != nil {
				return nil, handleError(chatId, err)
			}

			msg := tgbotapi.NewMessage(chatId, *msgText)
			msg.ParseMode = tgbotapi.ModeHTML
			_, err = bot.Send(msg)

			if err != nil {
				return nil, handleError(chatId, err)
			}

			if err := PutSummaryItem(*summaryItem); err != nil {
				return nil, handleError(chatId, err)
			}

			if err := deletePollDataForChat(chatData); err != nil {
				return nil, handleError(chatId, err)
			}

			if WEEK_DAY_NOW == 0 {
				summaryItem := WeekSummaryItem{
					WeekId:          weekId,
					DayId:           WEEK_FINAL_ID,
					ActivityOptions: make(map[int]int),
					ExerciseOptions: make(map[int]int),
				}
				weekSummaryItems, err := QuerySummaryItems(weekId)

				if err != nil {
					return nil, handleError(chatId, err)
				}

				for _, item := range weekSummaryItems {
					summaryItem.Points += item.Points
					for key, val := range item.ActivityOptions {
						summaryItem.ActivityOptions[key] += val
					}
					for key, val := range item.ExerciseOptions {
						summaryItem.ExerciseOptions[key] += val
					}
				}

				if err := PutSummaryItem(summaryItem); err != nil {
					return nil, handleError(chatId, err)
				}

				msgText := fmt.Sprintf("<b>Summary for week: %v\n\n</b>", TIME_NOW.Add(-time.Hour*168).Format("Jan 2 2006"))

				summaryText, _ := generateSummaryText(summaryItem)
				msgText += summaryText

				msg := tgbotapi.NewMessage(chatId, msgText)
				msg.ParseMode = tgbotapi.ModeHTML
				_, err = bot.Send(msg)

				if err != nil {
					return nil, handleError(chatId, err)
				}
			}
		}
	case SEND_LOVE_MESSAGE_COMMAND:
		chatIds, err := getChatIds()
		if err != nil {
			return nil, err
		}

		for _, chatId := range chatIds {
			msgText := generateReminderMessage()
			msg := tgbotapi.NewMessage(chatId, msgText)
			_, err = bot.Send(msg)

			if err != nil {
				return nil, handleError(chatId, err)
			}
		}
	case CLEAR_CACHE:
		if err := clearAllData(); err != nil {
			return nil, err
		}
	}
	return &CustomEventResponse{}, nil
}

func defaultHandler(update tgbotapi.Update, data ChatData) error {
	switch update.Message.Command() {
	case "start":
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("Welcome to SnailBot! %s", promptResponse()))
		bot.Send(msg)
		dumpChatData(data)
	case "summary":
		weekId := fmt.Sprintf("%v.%v.%v", data.ChatId, YEAR_NOW, WEEK_NOW)
		msgText, _, err := generateSummaryDaily(data, weekId)

		if err != nil {
			return err
		}
		log.Println("MSG TEXT", *msgText)
		msg := tgbotapi.NewMessage(data.ChatId, *msgText)
		msg.ParseMode = tgbotapi.ModeHTML
		output, err := bot.Send(msg)
		if err != nil {
			return err
		}
		log.Print(output)
	case "help":
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "How to use SnailBot 🐌:\n\n/start: start SnailBot 🐌\n/exit: exit the bot. Deletes all session data and disables scheduled messages.")
		bot.Send(msg)
	case "exit":
		if err := deleteChatData(data.ChatId); err != nil {
			return err
		}
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Thank you for using SnailBot 🐌 /start to restart bot.")
		bot.Send(msg)
	default:
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "It seems that this command lies beyond my shell of comprehension 🐌 /help to view possible commands.")
		bot.Send(msg)
	}
	return nil
}

func pollAnswerHandler(update tgbotapi.Update) error {
	log.Println("POLL_ANSWER", update.PollAnswer.OptionIDs)
	pollData, err := getPollData(update.PollAnswer.PollID)
	if err != nil {
		return err
	}
	pollData.SelectedOptions = update.PollAnswer.OptionIDs
	if err := dumpPollData(pollData); err != nil {
		return err
	}
	return nil
}

func processUpdate(update tgbotapi.Update) error {
	fromChat := update.FromChat()

	if fromChat != nil {
		data, err := getChatData(fromChat.ID)

		if err != nil {
			return handleError(data.ChatId, err)
		}

		switch data.ConvState {
		case DEFAULT_STATE:
			log.Println("EXECUTE_HANDLER", "Default handler")
			if err := defaultHandler(update, data); err != nil {
				return handleError(data.ChatId, err)
			}
		}

	} else if update.PollAnswer != nil {
		log.Println("EXECUTE_HANDLER", "Poll answer handler")
		return pollAnswerHandler(update)
	} else if update.Poll != nil {
		log.Println("UPDATE_PASS_THROUGH", "Poll handler")
	} else {
		updateJson, _ := json.Marshal(update)
		log.Fatalln("PROCESS_UPDATE_ERROR", fromChat, string(updateJson))
	}

	return nil
}
