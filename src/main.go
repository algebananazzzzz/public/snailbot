package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"reflect"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type Event struct {
	events.APIGatewayProxyRequest
	CustomEvent
}

func Handler(ctx context.Context, event Event) (interface{}, error) {
	switch {
	case !reflect.DeepEqual(event.APIGatewayProxyRequest, events.APIGatewayProxyRequest{}):
		log.Println("EXECUTING_HANDLER_INFO", "API_EVENT", event.APIGatewayProxyRequest)
		return ApiRequestHandler(ctx, event.APIGatewayProxyRequest)
	case !reflect.DeepEqual(event.CustomEvent, CustomEvent{}):
		log.Println("EXECUTING_HANDLER_INFO", "CUSTOM_EVENT", event.CustomEvent)
		return customEventHandler(event.CustomEvent)
	default:
		return nil, fmt.Errorf("unknown event: %v", event)
	}
}

func ApiRequestHandler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	log.Println("PROCESS_UPDATE_INFO", req.Body)

	if req.Body == "" {
		return events.APIGatewayProxyResponse{
			StatusCode: http.StatusOK,
			Body:       "Healthy",
		}, nil
	}

	var update tgbotapi.Update

	if err := json.Unmarshal([]byte(req.Body), &update); err != nil {
		log.Println("PROCESS_UPDATE_ERROR", err)
		return events.APIGatewayProxyResponse{StatusCode: http.StatusOK}, err
	}

	if err := processUpdate(update); err != nil {
		log.Println("PROCESS_UPDATE_ERROR", err)
		return events.APIGatewayProxyResponse{StatusCode: http.StatusOK}, err
	}

	return events.APIGatewayProxyResponse{StatusCode: http.StatusOK}, nil
}

func main() {
	if os.Getenv("LAMBDA_TASK_ROOT") == "" {
		bot.Debug = false
		u := tgbotapi.NewUpdate(0)
		u.Timeout = 60

		updates := bot.GetUpdatesChan(u)

		for update := range updates {
			processUpdate(update)
		}
	} else {
		lambda.Start(Handler)
	}
}
