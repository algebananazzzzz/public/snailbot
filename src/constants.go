package main

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/go-redis/redis"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const (
	DEFAULT_STATE = 0
	WEEK_FINAL_ID = 7
)

const (
	SEND_POLL_DAILY_COMMAND        = "send_poll_daily"
	GENERATE_SUMMARY_DAILY_COMMAND = "generate_summary_daily"
	SEND_LOVE_MESSAGE_COMMAND      = "send_love_message"
	CLEAR_CACHE                    = "clear_cache"
)

const (
	POLL_ACTIVITY_TYPE = 1
	POLL_EXERCISE_TYPE = 2

	POLL_ACTIVITY_QUESTION = "What activities did we do today"
	POLL_EXERCISE_QUESTION = "What exercises did we do today"
)

type PollOption struct {
	Option string
	Points int
}

var (
	POLL_ACTIVITY_OPTIONS = []PollOption{
		{Option: "Laze at home 🐌", Points: 2},
		{Option: "Food date 🌅🥞🍵", Points: 5},
		{Option: "Food date 🍔🍜🍲", Points: 5},
		{Option: "Food date 🌆🥩🍨", Points: 5},
		{Option: "Indoor activity 🌞👛🛍️", Points: 8},
		{Option: "Indoor activity 🌜🌃🎬🎮", Points: 8},
		{Option: "Outdoor activity 🌞🚴‍♂️🏞️", Points: 10},
		{Option: "Outdoor activity 🌜🌌🏕️", Points: 10},
	}

	POLL_EXERCISE_OPTIONS = []PollOption{
		{Option: "Swim 🏊‍♀️🏊", Points: 8},
		{Option: "Run 🏃‍♀️🏃‍♂️", Points: 8},
		{Option: "Sports 🏸🥏", Points: 8},
		{Option: "Daniel's Day exercise 👨🏃‍♂️🌞", Points: 3},
		{Option: "Daniel's Night exercise 👨🚴‍♂️🌜", Points: 3},
		{Option: "Casey's Day exercise 👩🏃‍♀️🌞", Points: 3},
		{Option: "Casey's Night exercise 👩🚴‍♀️🌜", Points: 3},
	}
)

var bot *tgbotapi.BotAPI
var redisClient *redis.Client
var dynamodbClient *dynamodb.Client

var (
	loc                *time.Location
	REDIS_KEY          string
	SUMMARY_TABLE_NAME string
	YEAR_NOW           int
	WEEK_NOW           int
	WEEK_DAY_NOW       int
	TIME_NOW           time.Time
)

func init() {
	// Load timezone
	loc, _ = time.LoadLocation("Asia/Singapore")
	TIME_NOW = time.Now().In(loc)
	YEAR_NOW, WEEK_NOW = TIME_NOW.ISOWeek()
	WEEK_DAY_NOW = int(TIME_NOW.Weekday())

	// Load environment variables
	if REDIS_KEY = os.Getenv("REDIS_KEY"); REDIS_KEY == "" {
		REDIS_KEY = "SnailBot"
	}

	if SUMMARY_TABLE_NAME = os.Getenv("SUMMARY_TABLE_NAME"); SUMMARY_TABLE_NAME == "" {
		SUMMARY_TABLE_NAME = "dev-db-dyntable-snailbot-summarytable"
	}

	// Load AWS SDK Clients
	cfg, err := config.LoadDefaultConfig(context.TODO())

	if err != nil {
		log.Fatalln("INIT__ERROR", "Unable to load config")
	}

	dynamodbClient = dynamodb.NewFromConfig(cfg)

	// Load telegram bot
	bot, err = tgbotapi.NewBotAPI(os.Getenv("TOKEN"))
	if err != nil {
		log.Fatalln("INIT_ERROR", "Failed to initialize telegram bot", err.Error())
	}

	// Load redis client
	if os.Getenv("LAMBDA_TASK_ROOT") == "" {
		redisClient = redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "",
			DB:       0,
		})
	} else {
		redisClient = redis.NewClient(&redis.Options{
			Addr:     os.Getenv("REDIS_ADDR"),
			Password: "",
			DB:       0,
		})
	}

	log.Println("INIT_SUCCESS", "Authorized on account %s", bot.Self.UserName)
	log.Println("INIT_SUCCESS", "redis key:", REDIS_KEY)
	log.Println("INIT_SUCCESS", "summary dynamodb table:", SUMMARY_TABLE_NAME)
}
